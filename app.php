<?php
/**
 * App.php
 *
 *
 * @package itcc-example-project
 * @author Liam Bailey
 */
require('./includes/class-webservice-connector.php');

?><section id="main"><?php
$rows = '';
if (isset($_GET['product_id'])) {
    $product_id = filter_var($_GET['product_id'], FILTER_SANITIZE_STRING);
    // instance method of using the class.
    $product = \WebserviceConnector\Webservice_Connector()->getProduct($product_id);
    if ($product !== false) {
        $product = $product[$product_id];
        if (is_array($product['suppliers'])) {
            $product_suppliers = array_map(function($val) {
                return filter_var($val, FILTER_SANITIZE_STRING);
            }, $product['suppliers']);
            $suppliers = "<li>" . implode("</li><li>", $product_suppliers) . "</li>";
        }
        ob_start();
        include("./htmlparts/single.html");
        $template = ob_get_contents();
        ob_end_clean();
        echo str_replace(array(
            '{{NAME}}',
            '{{TYPE}}',
            '{{DESCRIPTION}}',
            '{{SUPPLIERS}}'
        ), array(
            filter_var($product['name'], FILTER_SANITIZE_STRING),
            filter_var($product['type'], FILTER_SANITIZE_STRING),
            filter_var($product['description'], FILTER_SANITIZE_STRING),
            $suppliers
        ), $template);
    }
} else {
    // namespace method of using the class.
    $products = \WebserviceConnector\WebserviceConnector::getProducts();
    if ($products === false) {
        return;
    }
    $url_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
    $url_string = 'http://' . $_SERVER['HTTP_HOST'] . $url_parts[0] . "?product_id=%s";
    foreach ($products['products'] as $key => $product) {
        $key = filter_var($key, FILTER_SANITIZE_STRING);
        $product = filter_var($product, FILTER_SANITIZE_STRING);
        $product_link = '<a href="' . sprintf($url_string, $key) . '">'.$product.'</a>';
        $rows .= "\r\n";
        $rows .= sprintf("<li>%s</li>", $product_link);
        $rows .= "\r\n";
    }
    ob_start();
    include("./htmlparts/list.html");
    $template = ob_get_contents();
    ob_end_clean();
    echo str_replace('{{PRODUCTS}}', $rows, $template);
}
?></section><?php
