<?php
/**
 * Webservice connector for the itc api
 *
 * @package itcc-example-project
 * @author Liam Bailey
 */
namespace WebserviceConnector;

/**
 * Connects to the webservice including helper functions for requests
 *
 * @class Webservice_Connector
 * @package Itcc Example Project
 *
 */
class WebserviceConnector
{

    protected $api_url = 'https://www.itccompliance.co.uk/recruitment-webservice/api/';
    protected static $instance;

    /**
     * Instantiate the class easily
     *
     * @return Webservice_Connector
     */
    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Get details on single product from api
     *
     * @param string - product id (slug)
     *
     * @return array - the details of the product
     */
    public function getProduct($product_id)
    {
        $product = self::makeAPICall('info', 'GET', array('id' => $product_id));
        return $product;
    }

    /**
     * Gets the list of products from the ITC array
     *
     * @return array - list of products
     */
    public static function getProducts()
    {
        $products = array();
        $products = self::makeAPICall('list', 'GET');
        return $products;
    }

    /**
     * Makes api calls
     *
     * @param string $endpoint - the api endpoint to send request to
     * @param string $method - the method GET, POST, PUT etc.
     * @param array $data - the data to build into the request or the request body
     *
     * @return boolean|array - returns false on error and the response array on successful requests
     */
    protected static function makeAPICall($endpoint, $method, $data = array())
    {
        $curl = curl_init();
        $headers = array('Content-Type: application/json', 'Accept: application/json');
        $curl_opts =  array(
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_VERBOSE => 1,
            CURLOPT_URL => Webservice_Connector()->api_url . $endpoint,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HEADER => false,
        );
        if ($method == "GET") {
            $curl_opts[CURLOPT_CUSTOMREQUEST] = "GET";
        } else {
            $curl_opts[CURLOPT_POST] = 1;
        }
        curl_setopt_array($curl, $curl_opts);
        $response = json_decode(curl_exec($curl), true);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpcode > 200) {
            self::handleError(array('error' => 'Request Failed with code ' . $httpcode, 'body' => $response));
            return false;
        } else {
            if (array_key_exists('error', $response)) {
                if (strstr($response['error'], 'Data source error')) {
                    // Retry if API Fails.
                    $retries = 5;
                    while (array_key_exists('error', $response) && strstr($response['error'], 'Data source error')) {
                        $retries--;
                        if ($retries === 0) {
                            header("Location: " . $_SERVER['REQUEST_URI']);
                            exit;
                        }
                        return self::makeAPICall($endpoint, $method, $data);
                    }
                }
                self::handleError($response['error'], $response);
                return false;
            } else {
                return $response;
            }
        }
    }

    /**
     * Handles errors from the api
     *
     * @param string $error - the message to display to the user - often coming directly from the api
     * @param array $response - the full response from the array for logging
     *
     * @return void
     */
    protected static function handleError($error, $response)
    {
        ob_start();
        include("./htmlparts/error.html");
        $template = ob_get_contents();
        ob_end_clean();
        echo str_replace("{{ERROR}}", $error, $template);
        error_log("ITCC API ERROR: " . print_r($response, true));
        return;
    }
}

/**
 * Mobile instantiator for the \WebserviceConnector\WebserviceConnector class
 *
 * @return class WebserviceConnector
 */
function Webservice_Connector()
{
    return WebserviceConnector::instance();
}
